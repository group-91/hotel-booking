package com.trendyol.hotel.service.hotel;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.trendyol.hotel.contract.request.SearchRequest;
import com.trendyol.hotel.domain.Hotel;
import com.trendyol.hotel.domain.Room;
import com.trendyol.hotel.exception.HotelNotFoundException;
import com.trendyol.hotel.exception.RoomNotFoundException;
import com.trendyol.hotel.repository.hotel.HotelRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class HotelServiceImpl implements HotelService {

    private final HotelRepository hotelRepository;
    private final ObjectMapper objectMapper;

    @Override
    public void updateHotel(Hotel hotel) {
        hotelRepository.update(hotel);
    }

    @Override
    public Hotel applyPatchToHotel(JsonPatch patch, Hotel targetHotel) throws JsonPatchException, JsonProcessingException {
        Hotel patchedHotel = getPatched(patch, targetHotel);
        updateHotel(targetHotel);
        return patchedHotel;
    }

    private Hotel getPatched(JsonPatch patch, Hotel targetHotel) throws JsonPatchException, JsonProcessingException {
        JsonNode patched = patch.apply(objectMapper.convertValue(targetHotel, JsonNode.class));
        return objectMapper.treeToValue(patched, Hotel.class);
    }

    @Override
    public Hotel getHotelById(String id) {
        Optional<Hotel> optionalHotel = hotelRepository.findById(id);
        if(optionalHotel.isEmpty())
            throw new HotelNotFoundException();
        return optionalHotel.get();
    }

    @Override
    public List<Hotel> getSuitableHotels(SearchRequest searchRequest) {
        Optional<List<Hotel>> suitableHotels = hotelRepository.getSuitableHotels(searchRequest);
        if(suitableHotels.isEmpty())
            throw new HotelNotFoundException();
        return suitableHotels.get();    }

    @Override
    public void deleteHotel(String id) {
        getHotelById(id); // idli otel yoksa exception fırlatcak. controllerda yakalanıcak.
        hotelRepository.delete(id);
    }

    @Override
    public void createHotel(Hotel hotel) {
        hotelRepository.insert(hotel);
    }

}
