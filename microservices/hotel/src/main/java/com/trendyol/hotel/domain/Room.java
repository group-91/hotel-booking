package com.trendyol.hotel.domain;

import lombok.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
@Getter
@Setter
public class Room {

    @NonNull
    private String id;
    @NonNull
    private String hotelId;
    @NonNull
    private int roomNumber;
    private int floor;
    private int noOfBeds;
    private double m2;
    private String view;
    private double nightlyPrice;
    private List<Duration> bookedDates; //default value empty arraylist olcak

    public Room() {
        this.id = UUID.randomUUID().toString();
        this.hotelId = "randomHotel";
        this.roomNumber = 0;
        this.floor = 0;
        this.noOfBeds = 0;
        this.m2 = 0;
        this.view = "default view";
        this.nightlyPrice = 0;
    }

    public Room(@NonNull int roomNumber, int floor, int noOfBeds, double m2, String view, double nightlyPrice) {
        this.id = UUID.randomUUID().toString();
        this.hotelId ="placeHolderId";  //will change to hotel id from path
        this.roomNumber = roomNumber;
        this.floor = floor;
        this.noOfBeds = noOfBeds;
        this.m2 = m2;
        this.view = view;
        this.nightlyPrice = nightlyPrice;
    }
}
