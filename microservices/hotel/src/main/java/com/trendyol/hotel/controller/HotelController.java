package com.trendyol.hotel.controller;

import com.github.fge.jsonpatch.JsonPatch;
import com.trendyol.hotel.contract.request.SearchRequest;
import com.trendyol.hotel.domain.Hotel;
import com.trendyol.hotel.exception.HotelNotFoundException;
import com.trendyol.hotel.service.hotel.HotelService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/hotels")
public class HotelController {

    private final HotelService hotelService;

    @GetMapping
    public ResponseEntity<List<Hotel>> getHotels( @RequestParam(defaultValue = "0")  int pageNo,
                                                  @RequestParam(defaultValue = "10")  int pageSize,
                                                  @RequestParam(defaultValue = "19-10-2020") Date beginDate,
                                                  @RequestParam Date endDate,
                                                  @RequestParam(required = false) Optional<Double> lowPriceLimit,
                                                  @RequestParam(required = false) Optional<Double> highPriceLimit) {
        try {
            SearchRequest searchRequest = SearchRequest.builder()
                    .pageNo(pageNo)
                    .pageSize(pageSize)
                    .beginDate(beginDate)
                    .endDate(endDate)
                    .lowPriceLimit(lowPriceLimit.isEmpty() ? 0 : lowPriceLimit.get())
                    .highPriceLimit(highPriceLimit.isEmpty() ? Double.MAX_VALUE : highPriceLimit.get())
                    .build();
            List<Hotel> hotels = hotelService.getSuitableHotels(searchRequest);
            return ResponseEntity.ok(hotels);
        }
        catch (Exception e) {
            if(e instanceof HotelNotFoundException)
                return ResponseEntity.notFound().build();
            else
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity addHotel(@RequestBody Hotel hotel) {
        try {
            URI location = URI.create(String.format("/hotels/%s", hotel.getId()));
            hotelService.createHotel(hotel);
            return ResponseEntity.created(location).build();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Hotel> getHotelWithId(@PathVariable String id) {
        try {
            Hotel hotel = hotelService.getHotelById(id);
            return ResponseEntity.ok(hotel);
        }
        catch (Exception e) {
            if(e instanceof HotelNotFoundException)
                return ResponseEntity.notFound().build();
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    public ResponseEntity patchHotel(@PathVariable String id, @RequestBody JsonPatch patch) {
        try {
            Hotel hotel = hotelService.getHotelById(id);
            Hotel hotelPatched = hotelService.applyPatchToHotel(patch, hotel);
            return ResponseEntity.ok(hotelPatched);
        } catch (Exception e) {
            if(e instanceof HotelNotFoundException)
                return ResponseEntity.notFound().build();
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity deleteHotel(@PathVariable String id) {
        try {
            hotelService.deleteHotel(id);
            return ResponseEntity.noContent().build();
        }
        catch (Exception e) {
            if(e instanceof HotelNotFoundException)
                return ResponseEntity.notFound().build();
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
