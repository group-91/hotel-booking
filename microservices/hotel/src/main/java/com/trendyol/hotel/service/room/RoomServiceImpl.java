package com.trendyol.hotel.service.room;

import com.trendyol.common.model.BookingDTO;
import com.trendyol.common.saga.event.BookingCanceledEvent;
import com.trendyol.common.saga.event.BookingDoneEvent;
import com.trendyol.common.utils.UUIDGenerator;
import com.trendyol.hotel.contract.request.SearchRequest;
import com.trendyol.hotel.domain.Hotel;
import com.trendyol.hotel.domain.Room;
import com.trendyol.hotel.exception.HotelNotFoundException;
import com.trendyol.hotel.exception.RoomNotFoundException;
import com.trendyol.hotel.repository.hotel.HotelRepository;
import com.trendyol.hotel.repository.room.RoomRepository;
import com.trendyol.hotel.repository.roomBooking.RoomBookingRepository;
import com.trendyol.hotel.saga.publisher.EventPublisher;
import com.trendyol.hotel.service.hotel.HotelService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RoomServiceImpl implements RoomService {

	private final RoomRepository roomRepository;
	private final HotelRepository hotelRepository;
	private final HotelService hotelService;
	private final RoomBookingRepository roomBookingRepository;
	private final EventPublisher eventPublisher;


	@Override
	public void updateRoom(BookingDTO bookingDTO) {

		if(checkRoomAvailability(bookingDTO)){
			try{
				addBookedDate(bookingDTO);
			}
			catch (Exception e) {
				BookingCanceledEvent bookingCanceledEvent = BookingCanceledEvent.builder().bookingDTO(bookingDTO).transactionId(UUIDGenerator.getUUID()).build();
				eventPublisher.publishBookingCanceledEvent(bookingCanceledEvent);
			}
			BookingDoneEvent event = BookingDoneEvent.builder().bookingDTO(bookingDTO).transactionId(UUIDGenerator.getUUID()).build();
			eventPublisher.publishBookingDoneEvent(event);
		}

	}

	@Override
	public void addBookedDate(BookingDTO bookingDTO) {
		roomRepository.addBookedDate(bookingDTO);
	}

	private boolean checkRoomAvailability(BookingDTO bookingDTO) {
		return roomRepository.checkRoomAvailability(bookingDTO);
	}

	@Override
	public List<Room> getAvailableRoomsOfHotel(String id, SearchRequest searchRequest) {
		Optional<Hotel> hotel = hotelRepository.findById(id);
		if(hotel.isEmpty())
			throw new HotelNotFoundException();

		Optional<List<Room>> availableRooms = roomRepository.getAvailableRoomsOfHotel(hotel.get(), searchRequest);
		if(availableRooms.isEmpty())
			throw new RoomNotFoundException();
		return availableRooms.get();
	}

	@Override
	public void addRoomToHotel(Room room) {
		Hotel hotelToAdd = hotelService.getHotelById(room.getHotelId());

		hotelToAdd.arrangePriceRangeAdd(room.getNightlyPrice());

		hotelToAdd.addRoom(room);

		hotelRepository.update(hotelToAdd);
	}

	@Override
	public void deleteRoom(String hotelId, String roomId) {
		Hotel hotelToRemove = hotelService.getHotelById(hotelId);

		hotelToRemove.removeRoom(roomId);

		hotelToRemove.arrangePriceRangeRemove(roomId);

		hotelRepository.update(hotelToRemove);

	}

}
