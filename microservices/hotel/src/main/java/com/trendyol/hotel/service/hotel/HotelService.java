package com.trendyol.hotel.service.hotel;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.trendyol.hotel.contract.request.SearchRequest;
import com.trendyol.hotel.domain.Hotel;
import com.trendyol.hotel.domain.Room;

import java.util.List;
import java.util.Optional;

public interface HotelService {

	void createHotel(Hotel hotel);

	void updateHotel(Hotel hotelPatched);

	Hotel applyPatchToHotel(JsonPatch patch, Hotel targetHotel) throws JsonPatchException, JsonProcessingException;

	Hotel getHotelById(String id);

	List<Hotel> getSuitableHotels(SearchRequest searchRequest);

	void deleteHotel(String id);
}
