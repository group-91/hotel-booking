package com.trendyol.hotel.repository.hotel;

import com.couchbase.client.core.error.DocumentNotFoundException;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.kv.GetResult;
import com.couchbase.client.java.query.QueryResult;
import com.trendyol.common.exceptions.hotel.HotelCouldNotBeRemovedException;
import com.trendyol.hotel.contract.request.SearchRequest;
import com.trendyol.hotel.domain.Hotel;
import com.trendyol.hotel.domain.Location;
import com.trendyol.hotel.domain.Room;
import com.trendyol.hotel.exception.HotelCouldNotBeInsertedException;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.stereotype.Repository;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
@AllArgsConstructor
public class HotelRepositoryImpl implements HotelRepository{

    private final Cluster couchbaseCluster;
    private final Collection hotelCollection;
    private static final Format formatter = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public void insert(Hotel hotel) {
        try {
            hotelCollection.insert(hotel.getId(), hotel);
        }
        catch (Exception e) {
            throw new HotelCouldNotBeInsertedException(e.getMessage());
        }
    }

    @Override
    public void update(Hotel hotel) { hotelCollection.replace(hotel.getId(), hotel); }

    @Override
    public Optional<Hotel> findById(String id) {
        try {
            GetResult getResult = hotelCollection.get(id);
            Hotel hotel = getResult.contentAs(Hotel.class);
            return Optional.of(hotel);
        }
        catch (DocumentNotFoundException e) {
            return Optional.empty();
        }
    }

    /*
    @Override
    public Optional<List<Hotel>> findAll () {
        String statement = "Select id, name, location, noOfRooms, stars, rating, phone, email, priceRange from hotel";
        QueryResult query = couchbaseCluster.query(statement);
        List<Hotel> result = query.rowsAs(Hotel.class);
        if(result.size()==0)
            return Optional.empty();
        return Optional.of(result);
    }

     */

    @Override
    public Optional<List<Hotel>> getSuitableHotels(SearchRequest searchRequest) {
        String requestBeginDate = formatter.format(searchRequest.getBeginDate());
        String requestEndDate = formatter.format(searchRequest.getEndDate());
        // pricerange içinde ve o tarihlerde boş odası olan hotelleri getirecek
        String statement = String.format("Select id, name, location, stars, rating, phone, email, priceRange from hotel WHERE " +
                        "(\"%f\"<=priceRange.lowPriceLimit and (\"%f\">=priceRange.highPriceLimit) and " +
                        "room IN rooms SATISFIES" +
                        "( EVERY bookedDate IN room.bookedDates SATISFIES" +
                        "(\"%s\">=bookedDate.endDate ) or"+
                        "(\"%s\"<= bookedDate.beginDate ) )"+
                        " END;",
                searchRequest.getLowPriceLimit(), searchRequest.getHighPriceLimit(),
                requestBeginDate, requestEndDate
        );
        QueryResult query = couchbaseCluster.query(statement);
        List<Hotel> result = query.rowsAs(Hotel.class);
        if(result.size()==0)
            return Optional.empty();
        return Optional.of(result);
    }

    @Override
    public void delete(String id) {
        try {
            hotelCollection.remove(id);
        }
        catch (Exception e) {
            throw new HotelCouldNotBeRemovedException();
        }
    }
}
