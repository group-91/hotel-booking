package com.trendyol.hotel.domain;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Location {
    private String country;
    private String city;
    private String district;
}
