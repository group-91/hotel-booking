package com.trendyol.hotel.repository.room;

import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.query.QueryResult;
import com.trendyol.common.model.BookingDTO;
import com.trendyol.hotel.contract.request.SearchRequest;
import com.trendyol.hotel.domain.Hotel;
import com.trendyol.hotel.domain.Room;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.text.Format;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
@AllArgsConstructor
public class RoomRepositoryImpl implements RoomRepository {

	private final Cluster couchbaseCluster;
	private final Format dateFormatter;

	@Override
	public boolean checkRoomAvailability(BookingDTO bookingDTO) {
		return checkRoomAvailability(bookingDTO.getRoomId(),bookingDTO.getStartDate(),bookingDTO.getEndDate());
	}

	@Override
	public void addBookedDate(BookingDTO bookingDTO) {
			String requestBeginDate = dateFormatter.format(bookingDTO.getStartDate());
			String requestEndDate = dateFormatter.format(bookingDTO.getEndDate());
			String statement = String.format("UPDATE hotel SET room.bookedDates = ARRAY_APPEND (room.bookedDates, {\"startDate\":\"%s\" , \"endDate\":\"%s\"}) FOR room in rooms when room.id == \"%s\" END WHERE id = \"%s\"", requestBeginDate,requestEndDate,bookingDTO.getRoomId(),bookingDTO.getHotelId());
			couchbaseCluster.query(statement);
	}

	public boolean checkRoomAvailability(String roomId,Date startDate, Date endDate) {
		String requestBeginDate = dateFormatter.format(startDate);
		String requestEndDate = dateFormatter.format(endDate);

		//intersecting dates

		String statement = String.format("Select id, hotelId , floor, roomNumber, bookedDates from room WHERE id == \"%s\" and ANY bookedDates IN bookedDate SATISFIES"+
				"(\"%s\"<= bookedDate.beginDate and \"%s\">bookedDate.beginDate ) or"+
				"(\"%s\">= bookedDate.beginDate and \"%s\"<bookedDate.endDate )"+
				" END;", roomId,requestBeginDate,requestEndDate,requestBeginDate,requestBeginDate);
		QueryResult query = couchbaseCluster.query(statement);
		List<Room> result = query.rowsAs(Room.class);

		if(result.size()>0) return false;
		return true;
	}

	@Override
	public Optional<List<Room>> getAvailableRoomsOfHotel(Hotel hotel, SearchRequest searchRequest) {
		String requestBeginDate = dateFormatter.format(searchRequest.getBeginDate());
		String requestEndDate = dateFormatter.format(searchRequest.getEndDate());
		String statement = String.format("Select id, floor, roomNumber from room WHERE " +
						"hotelId == \"%s\" " +
						"and ( nightlyPrice between \"%f\" and \"%f\" ) " +
						"and EVERY bookedDate IN bookedDates SATISFIES" +
						"(\"%s\">=bookedDate.endDate ) or"+
						"(\"%s\"<= bookedDate.beginDate )"+
						" END;", hotel.getId(),
				searchRequest.getLowPriceLimit(), searchRequest.getHighPriceLimit(),
				requestBeginDate, requestEndDate
		);
		QueryResult query = couchbaseCluster.query(statement);
		List<Room> result = query.rowsAs(Room.class);
		if(result.size()==0)
			return Optional.empty();
		return Optional.of(result);
	}

}
