package com.trendyol.hotel.domain;

import com.trendyol.common.utils.UUIDGenerator;
import com.trendyol.hotel.exception.RoomNotFoundException;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
public class Hotel {
    @NonNull
    private String id;
    @NonNull
    private String name;
    private Location location;
    private int noOfRooms;
    private List<Room> rooms;
    private int stars; //maybe enum
    private double rating;
    private String phone;
    private String email;
    private PriceRange priceRange;

    public Hotel(@NonNull String name, Location location, int stars, double rating, String phone, String email) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.location = location;
        this.stars = stars;
        this.rating = rating;
        this.phone = phone;
        this.email = email;
    }

    public Hotel() {
        this.id = UUID.randomUUID().toString();
        this.name = "default hotel name";
        this.location = location;
        this.rooms = new ArrayList<Room>();
        this.noOfRooms = 0;
        this.priceRange = new PriceRange();
        this.stars = 0;
        this.rating = 0;
        this.phone = "+905000000000";
        this.email = "default@email.com";
    }

    public void arrangePriceRangeAdd(double nightlyPrice) {
        if(nightlyPrice < priceRange.lowPriceLimit )
            priceRange.lowPriceLimit = nightlyPrice;
        // in between nothing changes
        else if(nightlyPrice>=priceRange.highPriceLimit)
            priceRange.highPriceLimit = nightlyPrice;
    }

    public void arrangePriceRangeRemove(String roomId) {
        double nightlyPrice = getRoomWithId(roomId).getNightlyPrice();
        if(nightlyPrice == priceRange.lowPriceLimit || nightlyPrice == priceRange.highPriceLimit)
            reArrangePriceRange();
    }

    private Room getRoomWithId(String roomId) {
        List<Room> result = rooms.stream().filter(room -> room.getId().equals(roomId)).collect(Collectors.toList());
        if(result.size()==0)
            throw new RoomNotFoundException();
        return result.get(0);
    }

    private void reArrangePriceRange() {
        double lowPriceLimit = Double.MAX_VALUE;
        double highPriceLimit = 0;

        for(Room r : rooms) {
            if(r.getNightlyPrice() > highPriceLimit)
                highPriceLimit = r.getNightlyPrice();
            if(r.getNightlyPrice() < lowPriceLimit)
                lowPriceLimit = r.getNightlyPrice();
        }

        this.priceRange.lowPriceLimit = lowPriceLimit;
        this.priceRange.highPriceLimit = highPriceLimit;
    }

    public void addRoom(Room room) {
        rooms.add(room);
    }

    public void removeRoom(String roomId) {
        if(roomExists(id))
            rooms.removeIf(r -> r.getId().equals(roomId));
    }

    private boolean roomExists(String roomId) {
        getRoomWithId(roomId); //throws exception if room does not exist
        return true;
    }
}
