package com.trendyol.hotel.controller;

import com.trendyol.hotel.contract.request.SearchRequest;
import com.trendyol.hotel.domain.Hotel;
import com.trendyol.hotel.domain.Room;
import com.trendyol.hotel.exception.HotelNotFoundException;
import com.trendyol.hotel.exception.RoomNotFoundException;
import com.trendyol.hotel.service.room.RoomService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/hotels/{id}/rooms")
public class RoomController {

    private final RoomService roomService;

    @GetMapping
    public ResponseEntity<List<Room>> getAvailableRoomsOfHotel(@PathVariable String id,
                                                               @RequestParam(defaultValue = "0")  int pageNo,
                                                               @RequestParam(defaultValue = "10")  int pageSize,
                                                               @RequestParam(defaultValue = "19-10-2020") Date beginDate,
                                                               @RequestParam Date endDate,
                                                               @RequestParam(required = false) Optional<Double> lowPriceLimit,
                                                               @RequestParam(required = false) Optional<Double> highPriceLimit) {
        try {
            SearchRequest searchRequest = SearchRequest.builder()
                    .pageNo(pageNo)
                    .pageSize(pageSize)
                    .beginDate(beginDate)
                    .endDate(endDate)
                    .lowPriceLimit(lowPriceLimit.isEmpty() ? 0 : lowPriceLimit.get())
                    .highPriceLimit(highPriceLimit.isEmpty() ? Double.MAX_VALUE : highPriceLimit.get())
                    .build();
            List<Room> rooms = roomService.getAvailableRoomsOfHotel(id, searchRequest);
            return ResponseEntity.ok(rooms);
        }
        catch (Exception e) {
            if(e instanceof RoomNotFoundException || e instanceof HotelNotFoundException)
                return ResponseEntity.notFound().build();
            else
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity addRoomToHotel(@PathVariable("id") String hotelId, @RequestBody Room room) {
        try {
            URI location = URI.create(String.format("/hotels/%s/rooms/%s", hotelId, room.getId()));
            room.setHotelId(hotelId);
            roomService.addRoomToHotel(room);
            return ResponseEntity.created(location).build();
        }
        catch (Exception e) {
            if(e instanceof HotelNotFoundException)
                return ResponseEntity.notFound().build();
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping(path = "/{roomId}")
    public ResponseEntity deleteHotel(@PathVariable("id") String hotelId, @PathVariable("roomId") String roomId) {
        try {
            roomService.deleteRoom(hotelId, roomId);
            return ResponseEntity.noContent().build();
        }
        catch (Exception e) {
            if(e instanceof HotelNotFoundException || e instanceof RoomNotFoundException)
                return ResponseEntity.notFound().build();
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
