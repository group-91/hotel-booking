package com.trendyol.hotel.config;

import com.trendyol.common.constants.DatePatternConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.Format;
import java.text.SimpleDateFormat;

@Configuration
public class HotelConfig {
    @Bean
    public Format format() {
        return new SimpleDateFormat(DatePatternConstants.DATE_PATTERN);
    }
}
