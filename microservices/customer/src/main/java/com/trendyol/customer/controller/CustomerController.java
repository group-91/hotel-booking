package com.trendyol.customer.controller;


import com.trendyol.customer.entity.Customer;
import com.trendyol.customer.model.PageableResult;
import com.trendyol.customer.model.request.CustomerRequest;
import com.trendyol.customer.model.response.CustomerResponse;
import com.trendyol.customer.service.CustomerService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/customers")
public class CustomerController {


    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping
    public PageableResult<CustomerResponse> getCustomers(@PageableDefault Pageable pageable) {
        return customerService.getCustomers(pageable);
    }

    @GetMapping("/{id}")
    public CustomerResponse getCustomer(@PathVariable Long id) {
        return customerService.getCustomer(id);
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createCustomer(@RequestBody CustomerRequest customerRequest) {
        customerService.createCustomer(customerRequest);
    }

    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void editCustomer(@RequestBody CustomerRequest customerRequest, @PathVariable Long id) {
        customerService.updateCustomer(customerRequest, id);

    }
}
