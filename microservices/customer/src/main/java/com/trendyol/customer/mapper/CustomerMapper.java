package com.trendyol.customer.mapper;

import com.trendyol.customer.entity.Customer;
import com.trendyol.customer.model.request.CustomerRequest;
import com.trendyol.customer.model.response.CustomerResponse;
import org.springframework.stereotype.Component;

@Component
public class CustomerMapper {

    public CustomerResponse toCustomerResponse(Customer customer) {

        CustomerResponse customerResponse = new CustomerResponse();

        customerResponse.setName(customer.getName());
        customerResponse.setSurname(customer.getSurname());
        customerResponse.setPhone(customer.getPhone());
        customerResponse.setEmail(customer.getEmail());
        return customerResponse;
    }

    public Customer toCustomer(CustomerRequest customerRequest) {

        Customer customer = new Customer();
        customer.setName(customerRequest.getName());
        customer.setSurname(customerRequest.getSurname());
        customer.setPhone(customerRequest.getPhone());
        customer.setEmail(customerRequest.getEmail());
        return customer;
    }

    public Customer toCustomer(CustomerRequest customerRequest, Customer customer) {
        customer.setName(customerRequest.getName());
        customer.setSurname(customerRequest.getSurname());
        customer.setPhone(customerRequest.getPhone());
        customer.setEmail(customerRequest.getEmail());
        return customer;
    }


}
