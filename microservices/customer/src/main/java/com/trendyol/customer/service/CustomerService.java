package com.trendyol.customer.service;


import com.trendyol.customer.entity.Customer;
import com.trendyol.customer.mapper.CustomerMapper;
import com.trendyol.customer.model.PageableResult;
import com.trendyol.customer.model.request.CustomerRequest;
import com.trendyol.customer.model.response.CustomerResponse;
import com.trendyol.customer.repository.CustomerRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final CustomerMapper customerMapper;


    public CustomerService(CustomerRepository customerRepository, CustomerMapper customerMapper) {
        this.customerRepository = customerRepository;
        this.customerMapper = customerMapper;
    }

    public void createCustomer(CustomerRequest customerRequest) {
        Customer customer = customerMapper.toCustomer(customerRequest);
        customerRepository.save(customer);

    }

    public PageableResult<CustomerResponse> getCustomers(Pageable pageable) {
        Page<Customer> customerPage = customerRepository.findAll(pageable);
        List<CustomerResponse> customerResponses = customerPage.stream().map(customerMapper::toCustomerResponse).collect(Collectors.toList());
        return new PageableResult<>(customerPage.getTotalElements(), pageable.getPageNumber(), pageable.getPageSize(), customerResponses);
    }

    public CustomerResponse getCustomer(Long id) {
        Customer customer = getCustomerById(id);
        return customerMapper.toCustomerResponse(customer);

    }

    private Customer getCustomerById(Long id) {
        return customerRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }


    public void updateCustomer(CustomerRequest customerRequest, Long customerId) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException("Customer id not found: " + customerId));

        customer.setName(customerRequest.getName());
        customer.setSurname(customerRequest.getSurname());
        customer.setPhone(customerRequest.getPhone());
        customer.setEmail(customerRequest.getEmail());
        customerRepository.save(customer);
    }

}
