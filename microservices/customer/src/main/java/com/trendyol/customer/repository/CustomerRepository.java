package com.trendyol.customer.repository;

import com.trendyol.customer.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Page<Customer> findAll(Pageable pageable);

    Boolean existsByName(String name);

    Boolean existsByEmail(String email);


}
