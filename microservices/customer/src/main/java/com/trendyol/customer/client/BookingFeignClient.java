package com.trendyol.customer.client;


import com.trendyol.customer.entity.Booking;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "bookingClient", url = "localhost:8081/bookings")
public interface BookingFeignClient {

    @GetMapping("/customers/{customerId}")
    List<Booking> getBookings(@PathVariable Long customerId);

}
