package com.trendyol.customer.service;

import com.trendyol.customer.entity.Customer;
import com.trendyol.customer.mapper.CustomerMapper;
import com.trendyol.customer.model.PageableResult;
import com.trendyol.customer.model.request.CustomerRequest;
import com.trendyol.customer.model.response.CustomerResponse;
import com.trendyol.customer.repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {
    @InjectMocks
    private CustomerService customerService;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private CustomerMapper customerMapper;

    @Test
    void it_should_create_customer() {
        // Given
        CustomerRequest customerRequest = getCustomerRequest();
        Customer customer = getCustomer();

        when(customerMapper.toCustomer(customerRequest)).thenReturn(customer);
        // When
        customerService.createCustomer(customerRequest);

        // Then
        verify(customerMapper).toCustomer(customerRequest);
        verify(customerRepository).save(customer);

    }


    @Test
    void it_should_get_customers() {
        // Given
        PageRequest pageRequest = PageRequest.of(1, 10);
        Page<Customer> customers = Page.empty(pageRequest);

        when(customerRepository.findAll(pageRequest)).thenReturn(customers);
        // When
        PageableResult result = customerService.getCustomers(pageRequest);

        // Then
        verify(customerRepository).findAll(pageRequest);
        assertThat(result.getPage()).isEqualTo(1);
        assertThat(result.getSize()).isEqualTo(10);
    }

    @Test
    void it_should_get_customer() {
        // Given
        long id = 1L;
        CustomerResponse customerResponse = getCustomerResponse();
        Customer customer = getCustomer();
        when(customerRepository.findById(id)).thenReturn(Optional.of(customer));
        when(customerMapper.toCustomerResponse(customer)).thenReturn(customerResponse);

        // When
        CustomerResponse response = customerService.getCustomer(id);

        // Then
        verify(customerRepository).findById(id);
        verify(customerMapper).toCustomerResponse(customer);
        assertThat(response).isEqualTo(customerResponse);
    }

    @Test
    void it_should_edit_customer() {
        // Given
        Long id = 1L;
        CustomerRequest customerRequest = getCustomerEditRequest();
        Customer customer = getCustomer();
        when(customerRepository.findById(id)).thenReturn(Optional.of(customer));

        // When
        customerService.updateCustomer(customerRequest, id);

        // Then
        ArgumentCaptor<Customer> customerArgumentCaptor = ArgumentCaptor.forClass(Customer.class);
        verify(customerRepository).save(customerArgumentCaptor.capture());
        Customer capturedCustomer = customerArgumentCaptor.getValue();
        assertThat(capturedCustomer.getEmail()).isEqualTo(customerRequest.getEmail());
        assertThat(capturedCustomer.getName()).isEqualTo(customerRequest.getName());
        assertThat(capturedCustomer.getSurname()).isEqualTo(customerRequest.getSurname());
        assertThat(capturedCustomer.getPhone()).isEqualTo(customerRequest.getPhone());

    }


    private Customer getCustomer() {
        var customer = new Customer();
        customer.setName("name");
        customer.setSurname("surname");
        customer.setPhone("phone");
        customer.setEmail("email");
        return customer;
    }

    //
    private CustomerRequest getCustomerRequest() {
        var customerRequest = new CustomerRequest();
        customerRequest.setName("name");
        customerRequest.setSurname("surname");
        customerRequest.setPhone("phone");
        customerRequest.setEmail("email");
        return customerRequest;
    }

    private CustomerResponse getCustomerResponse() {
        var customerResponse = new CustomerResponse();
        customerResponse.setName("name");
        customerResponse.setSurname("surname");
        customerResponse.setPhone("phone");
        customerResponse.setEmail("email");
        return customerResponse;

    }

    private CustomerRequest getCustomerEditRequest() {
        var customerRequest = new CustomerRequest();
        customerRequest.setName("name1");
        customerRequest.setSurname("surname1");
        customerRequest.setPhone("phone1");
        customerRequest.setEmail("email1");
        return customerRequest;
    }


}
