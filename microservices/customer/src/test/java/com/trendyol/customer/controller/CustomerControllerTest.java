package com.trendyol.customer.controller;

import com.trendyol.customer.model.PageableResult;
import com.trendyol.customer.model.request.CustomerRequest;
import com.trendyol.customer.model.response.CustomerResponse;
import com.trendyol.customer.service.CustomerService;
import org.junit.jupiter.api.Test;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Pageable;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CustomerControllerTest {
    @InjectMocks
    private CustomerController customerController;

    @Mock
    private CustomerService customerService;

    @Test
    void it_should_create_customer() {
        // Given
        CustomerRequest customerRequest = new CustomerRequest();
        customerRequest.setName("ibrahim");
        customerRequest.setSurname("öztürk");
        customerRequest.setPhone("5462345678");
        customerRequest.setEmail("ibrahim@email.com");

        // When
        customerController.createCustomer(customerRequest);

        // Then
        ArgumentCaptor<CustomerRequest> customerRequestArgumentCaptor = ArgumentCaptor.forClass(CustomerRequest.class);
        verify(customerService).createCustomer(customerRequestArgumentCaptor.capture());
        CustomerRequest captorValue = customerRequestArgumentCaptor.getValue();
        assertThat(captorValue).isEqualToComparingFieldByField(customerRequest);
    }

    @Test
    void it_should_get_customers() {
        // Given
        Pageable pageable = Pageable.unpaged();
        PageableResult pageableResult = PageableResult.getEmptyPageableResult();
        when(customerService.getCustomers(pageable)).thenReturn(pageableResult);

        // When
        PageableResult result = customerController.getCustomers(pageable);

        // Then
        verify(customerService).getCustomers(pageable);
        assertThat(result).isEqualTo(pageableResult);
    }

    @Test
    void it_should_get_customer() {
        // Given
        long id = 1L;
        CustomerResponse customerResponse = new CustomerResponse();
        when(customerService.getCustomer(id)).thenReturn(customerResponse);

        // When
        CustomerResponse response = customerController.getCustomer(id);

        // Then
        verify(customerService).getCustomer(id);
        assertThat(response).isEqualTo(customerResponse);
    }

    @Test
    void it_should_edit_customer() {
        // Given
        long id = 1L;
        CustomerRequest customerRequest = new CustomerRequest();

        // When
        customerController.editCustomer(customerRequest, id);

        // Then
        verify(customerService).updateCustomer(customerRequest, id);
    }


}
