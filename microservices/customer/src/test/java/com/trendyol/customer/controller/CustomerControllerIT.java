package com.trendyol.customer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trendyol.customer.model.PageableResult;
import com.trendyol.customer.model.request.CustomerRequest;
import com.trendyol.customer.model.response.CustomerResponse;
import com.trendyol.customer.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CustomerController.class)

public class CustomerControllerIT {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CustomerService customerService;

    @Test
    void it_should_create_customer() throws Exception {
        //Given
        var customerRequest = new CustomerRequest();
        customerRequest.setName("ibrahim");
        customerRequest.setSurname("öztürk");
        customerRequest.setPhone("5462345678");
        customerRequest.setEmail("ibrahim@email.com");

        var objectMapper = new ObjectMapper();
        String request = objectMapper.writeValueAsString(customerRequest);

        //When
        mockMvc.perform(post("/customers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)).andExpect(status().isCreated());

        //Then
        var customerRequestArgumentCaptor = ArgumentCaptor.forClass(CustomerRequest.class);
        verify(customerService).createCustomer(customerRequestArgumentCaptor.capture());
        var capturedCustomerRequest = customerRequestArgumentCaptor.getValue();
        assertThat(capturedCustomerRequest).isEqualToComparingFieldByField(customerRequest);
    }


    @Test
    void it_should_get_customers() throws Exception {
        //Given
        var customerResponse = new CustomerResponse();
        customerResponse.setName("ibrahim");
        customerResponse.setSurname("öztürk");
        customerResponse.setPhone("5462345678");
        customerResponse.setEmail("ibrahim@email.com");

        PageableResult<CustomerResponse> pageableResult = new PageableResult<>(1, 0, 1, List.of(customerResponse));
        var objectMapper = new ObjectMapper();
        String expectedResponse = objectMapper.writeValueAsString(pageableResult);

        when(customerService.getCustomers(PageRequest.of(0, 1))).thenReturn(pageableResult);
        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("page", "0");
        params.add("size", "1");
        //When
        ResultActions resultActions = mockMvc.perform(get("/customers")
                .contentType(MediaType.APPLICATION_JSON)
                .params(params)).andExpect(status().isOk());

        //Then
        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.page").value(0))
                .andExpect(jsonPath("$.size").value(1))
                .andExpect(jsonPath("$.totalPages").value(1))
                .andExpect(jsonPath("$.content").isNotEmpty())
                .andExpect(jsonPath("$.content[0].email").value(customerResponse.getEmail()))
                .andExpect(jsonPath("$.content[0].surname").value(customerResponse.getSurname()))
                .andExpect(jsonPath("$.content[0].name").value(customerResponse.getName()))
                .andExpect(jsonPath("$.content[0].phone").value(customerResponse.getPhone()));
    }

    @Test
    void it_should_get_customer_by_id() throws Exception {
        //Given
        var customerResponse = new CustomerResponse();
        customerResponse.setName("ibrahim");
        customerResponse.setSurname("ozturk");
        customerResponse.setPhone("5462345678");
        customerResponse.setEmail("ibrahim@email.com");

        var objectMapper = new ObjectMapper();
        String expectedResponse = objectMapper.writeValueAsString(customerResponse);

        long customerId = 1L;
        when(customerService.getCustomer(customerId)).thenReturn(customerResponse);

        //When
        ResultActions resultActions = mockMvc.perform(get("/customers/" + customerId))
                .andExpect(status().isOk());

        //Then
        String response = resultActions.andReturn().getResponse().getContentAsString();
        assertThat(response).isEqualTo(expectedResponse);
    }

//    @Test
//    void it_should_edit_customer() throws Exception {
//        //Given
//        var customerRequest = new CustomerRequest();
//        customerRequest.setName("ibrahimm");
//        customerRequest.setSurname("ozturk");
//        customerRequest.setPhone("5462345678");
//        customerRequest.setEmail("ibrahimm@email.com");
//        long customerId = 1L;
//
//        var objectMapper = new ObjectMapper();
//        String request = objectMapper.writeValueAsString(customerRequest);
//
//        //When
//        mockMvc.perform(post("/customers/" + customerId)
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(request));
//
//        //Then
//        var customerRequestArgumentCaptor = ArgumentCaptor.forClass(CustomerRequest.class);
//        verify(customerService).updateCustomer(customerRequestArgumentCaptor.capture(), eq(customerId));
//        var capturedCustomerRequest = customerRequestArgumentCaptor.getValue().getEmail();
//        assertThat(capturedCustomerRequest).isEqualTo(customerRequest.getEmail());
//    }


}
