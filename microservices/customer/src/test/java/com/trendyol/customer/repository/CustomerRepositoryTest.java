package com.trendyol.customer.repository;

import com.trendyol.customer.entity.Customer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@DataJpaTest
class CustomerRepositoryTest {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    TestEntityManager testEntityManager;


    @Test
    void it_should_save() {
        // Given
        Customer customer = new Customer();
        customer.setPhone("12324");
        customer.setName("ibrahim");
        customer.setSurname("öztürk");
        customer.setEmail("ibrahim@gmail");

        // When
        Customer savedCustomer = customerRepository.save(customer);
        testEntityManager.flush();
        testEntityManager.clear();
        Optional<Customer> fetchedCustomer = customerRepository.findById(savedCustomer.getId());

        // Then
        assertThat(fetchedCustomer.isPresent()).isTrue();
        assertThat(fetchedCustomer.get()).isEqualToComparingOnlyGivenFields(customer, "name", "surname", "phone", "email");
    }

    @Test
    void it_should_throws_exception_when_not_null_constraint_violated() {
        // Given
        Customer customer = new Customer();
        customer.setPhone("12324");
        customer.setName("ibrahim");
        customer.setSurname("öztürk");

        // When
        Throwable throwable = catchThrowable(() -> {
            customerRepository.save(customer);
            testEntityManager.flush();
            testEntityManager.clear();
        });

        // Then
        assertThat(throwable).isNotNull();
        assertThat(throwable).isInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    void it_should_throws_exception_when_email_duplicated() {
        // Given
        Customer customer = new Customer();
        customer.setPhone("12324");
        customer.setName("ibrahim");
        customer.setSurname("öztürk");
        customer.setEmail("ibrahim@gmail");

        Customer customer2 = new Customer();
        customer.setPhone("12345");
        customer.setName("osman");
        customer.setSurname("yılmaz");
        customer.setEmail("osman@gmail");

        // When
        Throwable throwable = catchThrowable(() -> {
            customerRepository.save(customer);
            customerRepository.save(customer2);
            testEntityManager.flush();
            testEntityManager.clear();
        });

        // Then
        assertThat(throwable).isNotNull();
        assertThat(throwable).isInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    void it_should_test_exist_by_name() {
        // Given
        Customer customer = new Customer();
        customer.setPhone("12324");
        customer.setName("ibrahim");
        customer.setSurname("öztürk");
        customer.setEmail("ibrahim@gmail");

        // When
        testEntityManager.persistAndFlush(customer);
        testEntityManager.clear();
        Boolean isExist = customerRepository.existsByName(customer.getName());

        // Then
        assertThat(isExist).isTrue();
    }

    @Test
    void it_should_test_not_exist_by_name() {
        // Given

        // When
        Boolean isExist = customerRepository.existsByName("ibrahim");

        // Then
        assertThat(isExist).isFalse();
    }

    @Test
    void it_should_test_not_exist_by_email() {
        // Given

        // When
        Boolean isExist = customerRepository.existsByEmail("ibrahim@gmail.com");

        // Then
        assertThat(isExist).isFalse();
    }
}