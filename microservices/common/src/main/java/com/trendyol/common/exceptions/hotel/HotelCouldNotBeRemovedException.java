package com.trendyol.common.exceptions.hotel;

public class HotelCouldNotBeRemovedException extends RuntimeException {

    public HotelCouldNotBeRemovedException() {
        super();
    }

}
