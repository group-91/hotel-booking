package com.trendyol.common.exceptions.base;

import com.trendyol.common.enums.BaseExceptionType;
import lombok.Builder;

import java.util.List;
import java.util.function.Supplier;

public class RestResourceNotFoundException extends RestException {

    public RestResourceNotFoundException() {
        super(BaseExceptionType.REST_RESOURCE_NOT_FOUND_EXCEPTION, null, null);
    }

    @Builder
    private RestResourceNotFoundException(String message, List<String> errors) {
        super(BaseExceptionType.REST_RESOURCE_NOT_FOUND_EXCEPTION, message, errors);
    }
}
