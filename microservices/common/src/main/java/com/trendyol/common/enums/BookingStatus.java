package com.trendyol.common.enums;

public enum BookingStatus {
    NEW,
    DONE,
    CANCELED
}
