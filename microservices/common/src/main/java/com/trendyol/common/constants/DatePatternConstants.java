package com.trendyol.common.constants;

public class DatePatternConstants {
    public static final String DATE_PATTERN = "yyyy-MM-dd";
}
