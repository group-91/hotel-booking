package com.trendyol.common.model;

import com.trendyol.common.enums.BookingStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
@Builder
public class BookingDTO {

    private String id;
    private String hotelId;
    private String roomId;
    private String customerId;
    private BookingStatus bookingStatus;
    private Date startDate;
    private Date endDate;
}
