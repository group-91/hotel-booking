package com.trendyol.common.exceptions.hotel;

public class HotelCouldNotBeInsertedException extends RuntimeException {

    public HotelCouldNotBeInsertedException(String message) {
        super(message);
    }
}
