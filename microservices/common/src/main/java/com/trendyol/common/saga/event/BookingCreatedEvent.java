package com.trendyol.common.saga.event;

import com.trendyol.common.model.BookingDTO;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BookingCreatedEvent {

    private String transactionId;
    private BookingDTO bookingDTO;
}
