package com.trendyol.common.exceptions.hotel;

public class RoomNotFoundException extends RuntimeException {

    public RoomNotFoundException() {
        super("Room with id not found in the hotel.");
    }
}
