package com.trendyol.common.constants;

public class AmqpConstants {

    public static final String EXCHANGE_BOOKING_CREATED = "exchange-booking-created";

    public static final String QUEUE_BOOKING_CREATED = "queue-booking-created";

    public static final String QUEUE_BOOKING_CREATED_HOTEL = "queue-booking-created-hotel";

    public static final String EXCHANGE_BOOKING_CANCELED = "exchange-booking-canceled";

    public static final String QUEUE_BOOKING_CANCELED = "queue-booking-canceled";

    public static final String ROUTING_KEY_BOOKING_CANCELED = "rk-booking-canceled";

    public static final String EXCHANGE_BOOKING_DONE = "exchange-booking-done";

    public static final String QUEUE_BOOKING_DONE = "queue-booking-done";

    public static final String QUEUE_BOOKING_DONE_HOTEL = "queue-booking-done-hotel";

}
