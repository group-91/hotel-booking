package com.trendyol.common.exceptions.hotel;

public class HotelNotFoundException extends RuntimeException {

    public HotelNotFoundException() {
        super();
    }
}
