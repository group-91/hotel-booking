package com.trendyol.booking.client.hotel;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "hotelFeignClient", url = "localhost:8081/hotels")
public interface HotelFeignClient {

}
