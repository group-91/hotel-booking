package com.trendyol.booking.controller;

import com.trendyol.booking.service.booking.BookingService;
import com.trendyol.common.constants.URLConstants;
import com.trendyol.common.model.BookingDTO;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping (URLConstants.URL_BOOKING)
@Validated
@AllArgsConstructor
public class BookingController {

	private BookingService bookingService;

	@PostMapping
	public ResponseEntity<Void> createBooking(@Valid @RequestBody BookingDTO bookingDTO) {
		bookingService.publishBookingCreatedEvent(bookingDTO);
		return ResponseEntity.ok().build();
	}
}
