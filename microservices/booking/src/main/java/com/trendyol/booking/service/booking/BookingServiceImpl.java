package com.trendyol.booking.service.booking;

import com.trendyol.booking.mapper.BookingMapper;
import com.trendyol.booking.model.Booking;
import com.trendyol.booking.repository.BookingRepository;
import com.trendyol.booking.saga.publisher.EventPublisher;
import com.trendyol.booking.utils.UUIDGenerator;
import com.trendyol.common.enums.BookingStatus;
import com.trendyol.common.model.BookingDTO;
import com.trendyol.common.saga.event.BookingCreatedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Log4j2
@Service
@AllArgsConstructor
public class BookingServiceImpl implements BookingService {

	private final BookingRepository bookingRepository;
	private final BookingMapper bookingMapper;
	private final EventPublisher eventPublisher;

	@Transactional
	@Override
	public Booking createBooking(BookingDTO bookingDTO) {
		bookingDTO.setBookingStatus(BookingStatus.NEW);
		log.info("Saving an booking {}", bookingDTO);
		Booking booking = bookingMapper.toEntity(bookingDTO);
		return bookingRepository.save(booking);
	}

	@Transactional
	@Override
	public void cancelBooking(String bookingId) {
		log.info("Canceling Booking {}", bookingId);
		Optional<Booking> optionalBooking = bookingRepository.findById(bookingId);
		if(optionalBooking.isPresent()){
			Booking booking = optionalBooking.get();
			booking.setBookingStatus(BookingStatus.CANCELED);
			bookingRepository.save(booking);
		}
	}

	@Transactional
	@Override
	public void updateBookingAsDone(String bookingId) {
		log.info("Updating Booking {} to {}", bookingId, BookingStatus.DONE);
		Optional<Booking> bookingOptional = bookingRepository.findById(bookingId);
		if(bookingOptional.isPresent()){
			Booking booking = bookingOptional.get();
			booking.setBookingStatus(BookingStatus.DONE);
			bookingRepository.save(booking);
		}
	}

	public void publishBookingCreatedEvent(BookingDTO bookingDTO) {
		BookingCreatedEvent event = BookingCreatedEvent.builder().bookingDTO(bookingDTO).transactionId(UUIDGenerator.getUUID()).build();
		log.info("Booking created event published");
		eventPublisher.publishBookingCreatedEvent(event);
	}
}
