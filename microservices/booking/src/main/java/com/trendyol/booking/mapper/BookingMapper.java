package com.trendyol.booking.mapper;

import com.trendyol.booking.model.Booking;
import com.trendyol.common.model.BookingDTO;

public interface BookingMapper {

	BookingDTO toDTO(Booking entity);

	Booking toEntity(BookingDTO dto);
}
