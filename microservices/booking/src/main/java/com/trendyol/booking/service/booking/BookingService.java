package com.trendyol.booking.service.booking;

import com.trendyol.booking.model.Booking;
import com.trendyol.common.model.BookingDTO;

public interface BookingService {

	public Booking createBooking(BookingDTO bookingDTO);

	public void cancelBooking(String bookingId);

	public void updateBookingAsDone(String bookingId);

	public void publishBookingCreatedEvent(BookingDTO bookingDTO);

}
