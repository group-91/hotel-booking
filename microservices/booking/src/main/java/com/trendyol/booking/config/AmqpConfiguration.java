package com.trendyol.booking.config;

import com.trendyol.common.constants.AmqpConstants;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class AmqpConfiguration {

	@Bean
	@Lazy (false)
	public FanoutExchange bookingCreatedExchange() {
		return ExchangeBuilder.fanoutExchange(AmqpConstants.EXCHANGE_BOOKING_CREATED).durable(true).build();
	}

	@Bean
	@Lazy (false)
	public Queue bookingCreatedQueue() {
		return QueueBuilder.durable(AmqpConstants.QUEUE_BOOKING_CREATED).build();
	}

	@Bean
	@Lazy (false)
	public Queue bookingCreatedHotelQueue() {
		return QueueBuilder.durable(AmqpConstants.QUEUE_BOOKING_CREATED_HOTEL).build();
	}

	@Bean
	@Lazy (false)
	public Binding bookingCreatedBinding() {
		return BindingBuilder.bind(bookingCreatedQueue()).to(bookingCreatedExchange());
	}

	@Bean
	@Lazy (false)
	public Binding bookingCreatedHotelBinding() {
		return BindingBuilder.bind(bookingCreatedHotelQueue()).to(bookingCreatedExchange());
	}

	@Bean
	@Lazy (false)
	public Exchange bookingCanceledExchange() {
		return ExchangeBuilder.directExchange(AmqpConstants.EXCHANGE_BOOKING_CANCELED).durable(true).build();
	}

	@Bean
	@Lazy (false)
	public Queue bookingCanceledQueue() {
		return QueueBuilder.durable(AmqpConstants.QUEUE_BOOKING_CANCELED).build();
	}

	@Bean
	@Lazy (false)
	public Binding bookingCanceledBinding() {
		return BindingBuilder.bind(bookingCanceledQueue()).to(bookingCanceledExchange()).with(AmqpConstants.ROUTING_KEY_BOOKING_CANCELED).noargs();
	}

	@Bean
	@Lazy (false)
	public FanoutExchange bookingDoneExchange() {
		return ExchangeBuilder.fanoutExchange(AmqpConstants.EXCHANGE_BOOKING_DONE).durable(true).build();
	}

	@Bean
	@Lazy (false)
	public Queue bookingDoneQueue() {
		return QueueBuilder.durable(AmqpConstants.QUEUE_BOOKING_DONE).build();
	}

	@Bean
	@Lazy (false)
	public Queue bookingDoneHotelQueue() {
		return QueueBuilder.durable(AmqpConstants.QUEUE_BOOKING_DONE_HOTEL).build();
	}

}
