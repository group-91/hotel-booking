package com.trendyol.booking.repository;

import com.trendyol.booking.model.Booking;

import java.util.Optional;

public class BookingRepositoryImpl implements BookingRepository {

	@Override
	public <S extends Booking> S save(S s) {
		return null;
	}

	@Override
	public <S extends Booking> Iterable<S> saveAll(Iterable<S> iterable) {
		return null;
	}

	@Override
	public Optional<Booking> findById(String s) {
		return Optional.empty();
	}

	@Override
	public boolean existsById(String s) {
		return false;
	}

	@Override
	public Iterable<Booking> findAll() {
		return null;
	}

	@Override
	public Iterable<Booking> findAllById(Iterable<String> iterable) {
		return null;
	}

	@Override
	public long count() {
		return 0;
	}

	@Override
	public void deleteById(String s) {

	}

	@Override
	public void delete(Booking booking) {

	}

	@Override
	public void deleteAll(Iterable<? extends Booking> iterable) {

	}

	@Override
	public void deleteAll() {

	}
}
