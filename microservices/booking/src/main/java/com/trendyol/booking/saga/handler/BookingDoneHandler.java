package com.trendyol.booking.saga.handler;

import com.trendyol.booking.service.booking.BookingService;
import com.trendyol.common.constants.AmqpConstants;
import com.trendyol.common.converter.Converter;
import com.trendyol.common.saga.event.BookingDoneEvent;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@AllArgsConstructor
public class BookingDoneHandler {

	private final Converter converter;
	private final BookingService bookingService;

	@RabbitListener (queues = AmqpConstants.QUEUE_BOOKING_DONE)
	public void handleBookingDoneEvent(@Payload String payload) {
		log.debug("Handling a booking done event {}", payload);
		BookingDoneEvent event = converter.toObject(payload, BookingDoneEvent.class);
		bookingService.updateBookingAsDone(event.getBookingDTO().getId());
	}
}
